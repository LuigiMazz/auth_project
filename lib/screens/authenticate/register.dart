import 'package:auth_project/shared/constants.dart';
import 'package:flutter/material.dart';
import 'package:auth_project/services/auth.dart';

class Register extends StatefulWidget {
  final Function toggleView;
  Register({this.toggleView});

  @override
  _RegisterState createState() => _RegisterState();
}



class _RegisterState extends State<Register> {

  //MARK: - Final State 
  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();
  final focus = FocusNode();

  //textfield State
  String email = '';
  String password = '';
  String error = '';

  //MARK: - Function 
  Future _signUp() async {
    if (_formKey.currentState.validate()) {
      dynamic result = await _auth.registerWithEmailAndPassword(email, password);
      if (result == null) {
        setState(() => error = 'Please supply a valid email');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sign up'),
        elevation: 0.0,
        actions: <Widget>[
          FlatButton(
            child: Text('Sign in'),
            onPressed: () {
              widget.toggleView();
            },
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              SizedBox(height: 20.0),
              TextFormField(
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                decoration:
                    textInputDecoration.copyWith(hintText: 'Enter email'),
                validator: (value) =>  value.isEmpty ? 'Insert a valid Email' : null,
                onChanged: (value) {
                  setState(() => email = value);
                },
                onFieldSubmitted: (v){
                FocusScope.of(context).requestFocus(focus);
                },
              ),
              SizedBox(height: 20.0),
              TextFormField(
                 focusNode: focus,
                decoration: textInputDecoration.copyWith(hintText: 'Enter password'),
                validator: (value) => value.length < 6 ? 'Insert a password longher than 6 characters' : null,
                obscureText: true,
                onChanged: (value) {
                  setState(() => password = value);
                },
              ),
              SizedBox(height: 20.0),
              RaisedButton(
                onPressed: () async {_signUp();},
                child: Text('Sign up'),
              ),
              SizedBox(height: 20.0),
              Text(
                error,
                style: TextStyle(fontSize: 16.0, color: Colors.red),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
