import 'package:auth_project/services/auth.dart';
import 'package:auth_project/shared/constants.dart';
import 'package:flutter/material.dart';
import 'package:auth_project/shared/loading.dart';

class SignIn extends StatefulWidget {

  final Function toggleView;
    SignIn({this.toggleView});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();
  final focus = FocusNode();

  //textfield State
  String email = '';
  String password = '';
  String error = '';
  bool loading = false;

  Future _signIn() async {
    if (_formKey.currentState.validate()) {
                    setState(() => loading = true);
                   dynamic result = await _auth.signInWithEmailAndPassword(email, password);
                   if(result == null) {
                     setState(() {
                       error = 'Please supply a valid credential ';
                       loading = false;
                       }); 
                   }
                  }
  }
  @override
  Widget build(BuildContext context) {
    return loading ? Loading() : Scaffold(
      appBar: AppBar(
        title: Text('Sign In'),
        elevation: 0.0,
        actions: <Widget>[
          FlatButton(
            child: Text('Sign up'),
            onPressed: () {
              widget.toggleView();
            },
            )
        ],
      ),
      body: Container(
        padding:EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              SizedBox(height: 20.0),
              TextFormField(
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                decoration: textInputDecoration.copyWith(hintText: 'Insert Email'),
                validator: (value) => value.isEmpty ? 'Insert a valid Email' : null ,
                onChanged: (value) {
                  setState(() => email = value );
                },
                onFieldSubmitted: (v){
                FocusScope.of(context).requestFocus(focus);
              },
              ),
              SizedBox(height: 20.0),
              TextFormField(
                focusNode: focus,
                decoration: textInputDecoration.copyWith(hintText: 'Insert Password'),
                validator: (value) => value.length < 6 ? 'Insert a valid password' : null ,
                obscureText: true,
                onChanged: (value) {
                  setState(() => password = value );
                },
              ),
              SizedBox(height: 20.0),
              RaisedButton(
                onPressed: () async {
                  _signIn();
                },
                child: Text('Sign in'),
              ),
              SizedBox(height: 20.0),
              Text(error),
            ],
          ),
        ),
        ),
    );
  }
}