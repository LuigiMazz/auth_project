import 'package:auth_project/services/http_manager.dart';
import 'package:flutter/material.dart';

class NewPost extends StatefulWidget {
  @override
  _NewPostState createState() => _NewPostState();
}

class _NewPostState extends State<NewPost> {

  final HTTPManager _httpManager = HTTPManager();
  _newPost() async {
    var res = await _httpManager.newPost('title', 'body');
    print(res);
  }
  @override
  Widget build(BuildContext context) {
    _newPost();
    return Scaffold(
      appBar: AppBar(
        title: Text('add a new Post'),
      ),
    );
  }
}