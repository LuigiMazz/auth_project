import 'package:auth_project/models/jsonplaceholder.dart';
import 'package:auth_project/services/auth.dart';
import 'package:auth_project/services/http_manager.dart';
import 'package:auth_project/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:auth_project/shared/constants.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final AuthService _auth = AuthService();
  final HTTPManager _httpManager = HTTPManager();
  final _formKey = GlobalKey<FormState>();
  final focus = FocusNode();

  List<Post> postList;
  String title = '';
  String body= '';
  String error = '';
  

  _showDialog(BuildContext context) async {
    return showDialog(
    context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('new Post'),
          content: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              SizedBox(height: 20.0),
              TextFormField(
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                decoration: textInputDecoration.copyWith(hintText: 'Insert Title'),
                validator: (value) => value.isEmpty ? 'Insert a valid Title' : null ,
                onChanged: (value) {
                  setState(() => title = value );
                },
              ),
              SizedBox(height: 20.0),
              TextFormField(
                focusNode: focus,
                decoration: textInputDecoration.copyWith(hintText: 'Insert body'),
                validator: (value) => value.isEmpty ? 'Insert a valid body' : null ,
                onChanged: (value) {
                  setState(() => body = value );
                },
              ),  
              SizedBox(height: 20.0),
              Text(
                error,
                style: TextStyle(color: Colors.red, fontSize: 14.0),
                ),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Cancel'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          FlatButton(child: Text('Done'), onPressed: () async {
             if (_formKey.currentState.validate()) {
                var res = await _httpManager.newPost(title, body);
                  if (res == null ) {
                    setState(() {
                    error = 'Something was wrong';
                    });
                   }
                Navigator.of(context).pop();
              }
            },
          )
        ],
          );
      }
      );
  }
      
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _httpManager.getPosts().then((result) {
      setState(() {
        postList = result;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (postList == null) {
      return Loading();
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text('Home'),
          elevation: 0.0,
          actions: <Widget>[
            FlatButton.icon(
              onPressed: () async {
                await _auth.signOut();
              },
              icon: Icon(Icons.person),
              label: Text('Sign out'),
            ),
          ],
        ),
        body: ListView.builder(
            itemCount: postList.length,
            itemBuilder: (context, index) {
              return Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 2.0, horizontal: 5.0),
                child: Card(
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 5.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 20.0),
                        Text(
                          postList[index].title,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 20.0, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 30.0),
                        Text(
                          postList[index].body,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            fontSize: 18.0,
                          ),
                        ),
                        SizedBox(height: 10.0),
                      ],
                    ),
                  ),
                ),
              );
            }),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            _showDialog(context);
          },
        ),
      );
    }
  }
}
