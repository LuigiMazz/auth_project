import 'dart:convert';

import 'package:auth_project/models/jsonplaceholder.dart';
import 'package:http/http.dart';

class HTTPManager {

  //Get list of post 
   Future<List<Post>> getPosts() async {
    List<Post> list;
      var res = await get('https://jsonplaceholder.typicode.com/posts');
    if (res.statusCode >= 200 || res.statusCode <= 299) {
        try {
          var data = jsonDecode(res.body);
          var rest = data as List;
          list = rest.map<Post>((json) => Post.fromJson(json)).toList();
        } catch (error) {
          print(error);
          return null;
        }   
      }
      return list; 
   }
  

  //Post a new Post
  Future newPost(String title, String body) async {
    dynamic resp;
     Map<String, String> headers = {"Content-type": "application/json"};
     String json = '{"title": "$title", "body": "$body", "userId": 1}'; 
     print(json);
    var res = await post('https://jsonplaceholder.typicode.com/posts',headers: headers, body: json);
    if (res.statusCode >= 200 || res.statusCode <= 299){
      try {
        var data = jsonDecode(res.body);
        resp = data;
      } catch(error) {
        print('error: $error');
      }
    }
    return resp;
  }

}